import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class servletJava extends HttpServlet {
    private String message;

    public void init() throws ServletException {
        message = "Welcome to this Servlet";
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<H1>" + message + "<H1>");
    }
    public void destroy(){
        //ends servlet
    }
}